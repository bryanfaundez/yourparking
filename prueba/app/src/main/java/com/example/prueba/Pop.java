package com.example.prueba;

import android.app.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class Pop extends Activity {

    int precio_por_minuto;
    int hora_inicio;
    int minuto_inicio;


    TextView texto_precio_minuto;
    TextView resultado;
    Button boton1;
    Button boton2;

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putInt("hora_inicio", hora_inicio);
        savedInstanceState.putInt("minuto_inicio", minuto_inicio);
        savedInstanceState.putInt("visibilidad_boton1", boton1.getVisibility());
        savedInstanceState.putInt("visibilidad_boton2", boton2.getVisibility());

        // etc.
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        hora_inicio  = savedInstanceState.getInt("hora_inicio");
        minuto_inicio = savedInstanceState.getInt("minuto_inicio");
        boton1.setVisibility(savedInstanceState.getInt("visibilidad_boton1"));
        boton2.setVisibility(savedInstanceState.getInt("visibilidad_boton2"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.popupwindow);

        texto_precio_minuto = findViewById(R.id.precio);
        resultado = findViewById(R.id.resultado);
        boton1 = findViewById(R.id.button8);
        boton2 = findViewById(R.id.button9);


        boton2.setVisibility(View.INVISIBLE);

        resultado.setText("");



        Bundle datos = this.getIntent().getExtras();

        precio_por_minuto = datos.getInt("variable_integer");

        texto_precio_minuto.setText("Precio por minuto: "+ precio_por_minuto);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.9), (int)(height*.2));

        //Toast toast1 = Toast.makeText(getApplicationContext(), "minuto inicial="+minuto_inicio , Toast.LENGTH_SHORT);
        //toast1.show();

        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boton1.setVisibility(View.INVISIBLE);
                boton2.setVisibility(View.VISIBLE);
                Date date = new Date();
                final Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                hora_inicio = cal.get(Calendar.HOUR_OF_DAY);
                minuto_inicio = cal.get(Calendar.MINUTE);

                //Toast toast1 = Toast.makeText(getApplicationContext(), "minuto inicial="+minuto_inicio , Toast.LENGTH_SHORT);
                //toast1.show();
            }
        });

        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boton1.setVisibility(View.VISIBLE);
                boton2.setVisibility(View.INVISIBLE);

                Date date2 = new Date();

                final Calendar cal2 = Calendar.getInstance();
                cal2.setTime(date2);

                int tiempo_transcurrido = (cal2.get(Calendar.HOUR_OF_DAY) - hora_inicio)*60 + (cal2.get(Calendar.MINUTE) - minuto_inicio);
                resultado.setText("Tiempo estacionado: "+ tiempo_transcurrido + " | " + "Costo: $"+tiempo_transcurrido*precio_por_minuto);

                //Toast toast1 = Toast.makeText(getApplicationContext(), "minuto inicial="+minuto_inicio+"///minuto actual="+cal2.get(Calendar.MINUTE)+"///////"+ (cal2.get(Calendar.MINUTE) - minuto_inicio)+"", Toast.LENGTH_SHORT);
                //toast1.show();
            }
        });
    }
}
