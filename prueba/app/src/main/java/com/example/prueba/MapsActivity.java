package com.example.prueba;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

//import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.i("Permisos", "Se tienen los permisos!");
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1222);
            }
        }
        SystemClock.sleep(1000);
        mMap.setMyLocationEnabled(true);


        final PolylineOptions calle1 = new PolylineOptions()

                .add(new LatLng(-33.01529035463491, -71.55137136790673)) // P1
                .add(new LatLng(-33.01608834521381, -71.55153087976205)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));     // verde

        PolylineOptions calle2 = new PolylineOptions()
                .add(new LatLng(-33.01450788435859, -71.5484516368275)) // P1
                .add(new LatLng(-33.01464887242778, -71.54741823970932)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde

        PolylineOptions calle3 = new PolylineOptions()
                .add(new LatLng(-33.01779446733382, -71.54797208977347)) // P1
                .add(new LatLng(-33.01875805698862, -71.54818853524807)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde


        PolylineOptions calle4 = new PolylineOptions()
                .add(new LatLng(-33.01122617504929, -71.54178096793774)) // P1
                .add(new LatLng(-33.01110904474081, -71.542745880342)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde


        PolylineOptions calle5 = new PolylineOptions()
                .add(new LatLng(-33.01309997980522, -71.54329520122161)) // P1
                .add(new LatLng(-33.012957272374884, -71.54425529704643)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde


        PolylineOptions calle6 = new PolylineOptions()
                .add(new LatLng(-33.01146412399403, -71.54113578896683)) // P1
                .add(new LatLng(-33.01223668924612, -71.5413135669547)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde


        PolylineOptions calle7 = new PolylineOptions()
                .add(new LatLng(-33.01050993471654, -71.54256167803517)) // P1
                .add(new LatLng(-33.01062659476479, -71.54174070992529)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde



        PolylineOptions calle8 = new PolylineOptions()
                .add(new LatLng(-33.01962349591883, -71.54616935315073)) // P1
                .add(new LatLng(-33.01928193361229, -71.54606404027925)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde



        PolylineOptions calle9 = new PolylineOptions()
                .add(new LatLng(-33.00977298878194, -71.53944091316951)) // P1
                .add(new LatLng(-33.01071124900017, -71.53908416993693)) // P2
                .width(20)
                .color(Color.parseColor("#83FF33"));    // verde









        final Polyline polyline1 = mMap.addPolyline(calle1);
        final Marker calle_prueba1 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline1.getPoints().get(0).latitude +  polyline1.getPoints().get(1).latitude)  /2), ((polyline1.getPoints().get(0).longitude +  polyline1.getPoints().get(1).longitude)  /2)))
                .title("Calle 1")
                .snippet("capacidad: 5 estacionamientos \r Libres: 5")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));





        final Polyline polyline2 = mMap.addPolyline(calle2);
        final Marker calle_prueba2 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline2.getPoints().get(0).latitude +  polyline2.getPoints().get(1).latitude)  /2), ((polyline2.getPoints().get(0).longitude +  polyline2.getPoints().get(1).longitude)  /2)))
                .title("Calle 2")
                .snippet("capacidad: 3 estacionamientos \r Libres: 3")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));




        final Polyline polyline3 = mMap.addPolyline(calle3);
        final Marker calle_prueba3 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline3.getPoints().get(0).latitude +  polyline3.getPoints().get(1).latitude)  /2), ((polyline3.getPoints().get(0).longitude +  polyline3.getPoints().get(1).longitude)  /2)))
                .title("Calle 3")
                .snippet("capacidad: 7 estacionamientos \r Libres: 7")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));



        final Polyline polyline4 = mMap.addPolyline(calle4);
        final Marker calle_prueba4 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline4.getPoints().get(0).latitude +  polyline4.getPoints().get(1).latitude)  /2), ((polyline4.getPoints().get(0).longitude +  polyline4.getPoints().get(1).longitude)  /2)))
                .title("Calle 4")
                .snippet("capacidad: 7 estacionamientos \r Libres: 7")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));


        final Polyline polyline5 = mMap.addPolyline(calle5);
        final Marker calle_prueba5 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline5.getPoints().get(0).latitude +  polyline5.getPoints().get(1).latitude)  /2), ((polyline5.getPoints().get(0).longitude +  polyline5.getPoints().get(1).longitude)  /2)))
                .title("Calle 4")
                .snippet("capacidad: 7 estacionamientos \r Libres: 7")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));



        final Polyline polyline6 = mMap.addPolyline(calle6);
        final Marker calle_prueba6 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline6.getPoints().get(0).latitude +  polyline6.getPoints().get(1).latitude)  /2), ((polyline6.getPoints().get(0).longitude +  polyline6.getPoints().get(1).longitude)  /2)))
                .title("Calle 4")
                .snippet("capacidad: 7 estacionamientos \r Libres: 7")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));

        final Polyline polyline7 = mMap.addPolyline(calle7);
        final Marker calle_prueba7 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline7.getPoints().get(0).latitude +  polyline7.getPoints().get(1).latitude)  /2), ((polyline7.getPoints().get(0).longitude +  polyline7.getPoints().get(1).longitude)  /2)))
                .title("Calle 4")
                .snippet("capacidad: 7 estacionamientos \r Libres: 7")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));


        final Polyline polyline8 = mMap.addPolyline(calle8);
        final Marker calle_prueba8 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline8.getPoints().get(0).latitude +  polyline8.getPoints().get(1).latitude)  /2), ((polyline8.getPoints().get(0).longitude +  polyline8.getPoints().get(1).longitude)  /2)))
                .title("Calle 4")
                .snippet("capacidad: 7 estacionamientos \r Libres: 7")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));


        final Polyline polyline9 = mMap.addPolyline(calle9);
        final Marker calle_prueba9 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((polyline9.getPoints().get(0).latitude +  polyline9.getPoints().get(1).latitude)  /2), ((polyline9.getPoints().get(0).longitude +  polyline9.getPoints().get(1).longitude)  /2)))
                .title("Calle 4")
                .snippet("capacidad: 7 estacionamientos \r Libres: 7")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.estacionamiento)));





        final int precio_por_minuto = 22 ;

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker m) {
                if (m.equals(calle_prueba1)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba2)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba3)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba4)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba5)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba6)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba7)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba8)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                if (m.equals(calle_prueba9)) {
                    Intent intent = new Intent(new Intent(MapsActivity.this,Pop.class));
                    intent.putExtra("variable_integer", precio_por_minuto);

                    startActivity(intent);
                }
                return false;
            }
        });





        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();




        //Location yo = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria,false));



        LatLng vina = new LatLng(-33.01294840792019, -71.54181388405436);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(vina,16));

    }
}
